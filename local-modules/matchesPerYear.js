const fs = require('fs')

function matchesPerYear(dataArray){
  let outputObj = {}

  for(let counter = 0; counter < dataArray.length; counter++){

    let year = dataArray[counter]['season']

    if(outputObj.hasOwnProperty(year)){
      outputObj[year] += 1
    }
    else{
      outputObj[year] = 1
    }
  }

  //writing the data into a file
  fs.writeFile('/home/aditya/Documents/IPL_Project/src/public/output/matchesPerYear.json', JSON.stringify(outputObj), () => {
    console.log('matchesPerYear: Done')
  })
}

module.exports = matchesPerYear
