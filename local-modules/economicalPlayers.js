const fs = require('fs')

function economicalPlayers(arrayObject){

  let outerObj = {}
  let economyRateObj = {}

  /*
  Value of counter is calculated from row number of the
  id of the first match of 2015. The matches that were played inspect
  in 2015 have ids from 518 to 576. In deliveries csv file, these ids are
  present from row number 122714 to 136365. Their respective indices
  in the parsed array would be (row number - 2)
  */
  for(let counter = 122712; counter <= 136363; counter++){

    let player = arrayObject[counter]['bowler']
    let tempTotal = arrayObject[counter]['total_runs']
    let tempOver = arrayObject[counter]['over']

    if(outerObj.hasOwnProperty(player)){
      outerObj[player]['totalRuns'] += parseInt(tempTotal)

      if(outerObj[player]['lastOver'] !== tempOver){
        outerObj[player]['totalOvers'] += 1
        outerObj[player]['lastOver'] = tempOver
      }
    }
    else{
      let tempObj = {}
      tempObj['totalRuns'] = parseInt(tempTotal)
      tempObj['totalOvers'] = 1
      tempObj['lastOver'] = tempOver
      outerObj[player] = tempObj
    }
  }

  for(let element in outerObj){
    let economyRate = outerObj[element]['totalRuns']/outerObj[element]['totalOvers']
    economyRateObj[element] = economyRate
  }

  let sortedValues = Object.entries(economyRateObj).sort((a,b) => a[1]-b[1])
  let top10 = sortedValues.slice(0,10)

  fs.writeFile('/home/aditya/Documents/IPL_Project/src/public/output/top10EconomicalBowlers.json', JSON.stringify(top10), () => {
    console.log('economicalPlayers: Done')
  })
}

module.exports = economicalPlayers
