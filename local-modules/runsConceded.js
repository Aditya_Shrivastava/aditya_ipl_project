const fs = require('fs')

function runsConceded(arrayObject){

  let obj = {}
  /*
  Calculated the value of counter from row number of match ids in the
  csv file. The first match that starts in 2016 has a id of 577. In the
  delieveries csv file, that id starts from row number 136366. Its index
  in the parsed array would be 136364 (row number - 2).
  */
  for(let counter = 136364; counter < arrayObject.length; counter++){

    let team = arrayObject[counter]['bowling_team']
    let extras = arrayObject[counter]['extra_runs']

    if(obj.hasOwnProperty(team)){
      obj[team] += parseInt(extras)
    }
    else{
      obj[team] = parseInt(extras)
    }
  }

  // Writing the output in an external file.
  fs.writeFile('/home/aditya/Documents/IPL_Project/src/public/output/runsConceded.json', JSON.stringify(obj), () => {
    console.log('runsConceded: Done')
  })
}
module.exports = runsConceded
