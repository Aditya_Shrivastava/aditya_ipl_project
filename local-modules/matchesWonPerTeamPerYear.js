const fs = require('fs')

function matchesWonPerTeamPerYear(arrayObject){
  const outerObj = {}

  for(let counter = 0; counter < arrayObject.length; counter++){

    let winnerTeam = arrayObject[counter]['winner']
    let year = arrayObject[counter]['season']

    /*
    Checking whether the outerObj already has the team.
    If yes, then check for the year.
    If not, then insert it in the object along with the year.
    */

    if(outerObj.hasOwnProperty(winnerTeam)){

      let innerObj = outerObj[winnerTeam]

      /*
      Checking if the team has already won in that year.
      If yes, then increment the count. If no, then insert the year.
      */
      if(innerObj.hasOwnProperty(year)){
        innerObj[year] += 1
      }
      else{
        innerObj[year] = 1
      }

    }
    else{
      let innerObj = {}
      innerObj[year] = 1
      outerObj[winnerTeam] = innerObj
    }
  }

  // Writing the data into a file.
  fs.writeFile('/home/aditya/Documents/IPL_Project/src/public/output/matchesWonPerTeamPerYear.json', JSON.stringify(outerObj), () => {
    console.log('matchesWonPerTeamPerYear: Done')
  })
}

module.exports = matchesWonPerTeamPerYear
