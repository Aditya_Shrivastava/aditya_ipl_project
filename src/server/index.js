const fs = require('fs')
const csv = require('csv-parser')

// Importing local modules.
const matchesPerYear = require('/home/aditya/Documents/IPL_Project/local-modules/matchesPerYear.js')
const matchesWonPerTeamPerYear = require('/home/aditya/Documents/IPL_Project/local-modules/matchesWonPerTeamPerYear.js')
const runsConceded = require('/home/aditya/Documents/IPL_Project/local-modules/runsConceded.js')
const economicalPlayers = require('/home/aditya/Documents/IPL_Project/local-modules/economicalPlayers.js')

const arrayOfDataMatches = []
const arrayOfDataDeliveries = []


// Reading matches.csv and parsing it. Then using relevant functions.
fs.createReadStream('/home/aditya/Documents/IPL_Project/src/data/matches.csv')
.pipe(csv())
.on('data', function(data){
  arrayOfDataMatches.push(data)
})
.on('end', function(){
  matchesPerYear(arrayOfDataMatches)
  matchesWonPerTeamPerYear(arrayOfDataMatches)
})


// Reading deliveries.csv and parsing it. Then using relevant functions.
fs.createReadStream('/home/aditya/Documents/IPL_Project/src/data/deliveries.csv')
.pipe(csv())
.on('data', function(data){
  arrayOfDataDeliveries.push(data)
})
.on('end', function(){
  runsConceded(arrayOfDataDeliveries)
  economicalPlayers(arrayOfDataDeliveries)
})
